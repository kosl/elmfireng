.. ELMFIREng documentation master file, created by
   sphinx-quickstart on Tue Jun 11 11:18:12 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ELMFIREng's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 3

   code
   input
   howto
   simpic

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
