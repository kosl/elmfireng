Source file description for ELMFIRE v12 and v14b
------------------------------------------------


:file:`constants.f90`
~~~~~~~~~~~~~~~~~~~~~~

Useful constants (in IS units).


:file:`elmCollisions.f90`
~~~~~~~~~~~~~~~~~~~~~~~~~

  **subroutine bccoor(prts,iori,nprts)**

Coordination of particles for binary collisions

*k*-is the slot number *(i-1)*mct+j* of *ith* radial and *jth* poloidal slot

*nummc(k)*- is the number of particles in slot *k* so far

*numc(ll,k)*-identifies llth particle in kth slot.

  **subroutine bincol(p1,p2)**

This subroutine performs pairwise binary collisions between particle
species *p1* and *p2* using the stored (arrays *nummc* and *numc*)
information of particle numbers in each binary collision cell (mcrt
cells in total). Species have to be arranged in growing plznum, i.e.,
*plznum(3)>plznum(2)*. If the charge ratio of the *p2* and *p1*
particles is not one, collison of the *p1* particle is performed with
a probability *probab = plznum(p1)/plznum$* and the collision
frequency is divided by this probability. Random pairing is used in
each cell.


:file:`elmDiagnostics.f90`
~~~~~~~~~~~~~~~~~~~~~~~~~~

Samples diagnostic quantities from particle parameters.


:file:`elmFields.f90`
~~~~~~~~~~~~~~~~~~~~~

**subroutine erep(rs,theXinp,zetinp,imode,useTheva)**

Calculation of E field

::

    Er = -dPhi/dr (theva const)
    Ep = -dPhi/dtheXa
    Ez = -dPhi/dz, where z is angle


Input: potential table, radius, poloidal angle, zet, interpmode

Interpolation (and input) poloidal angle can be theta or theva

For some reason using diffrent w() arrays improves performance.

**subroutine eprep(rs,thet,zetinp,imode)**

Calculation of electric field for polarization. It has to be
consistent with ibmatxy for implicit calculations. So far only works
with compatibility with imode=0 (imode=1 removed). It uses three point
interpolation on the derivative direction.

Input: potential, radius, theta, zet, interp mode

Output: *Er = -dPhi/dr*; *Ep = -dPhi/dtheta*.


:file:`elmFileControl.f90`
~~~~~~~~~~~~~~~~~~~~~~~~~~

This module takes care of file management


:file:`elmfire.f90`
~~~~~~~~~~~~~~~~~~~

This main program of elmfire controls the mpi operations, calls the
initialization subroutine initia, performs the particle
initialization, and runs through the time loop (85 continue)
conducting binary collisions and particle loops (do 600 and do 1600)
as well as the inversion of the poisson equation (call spaslv) and the
output diagnostics (call outen1) at each time step. some key variables
are:

lrep = an index pointing the time step; lrep=0 is the first step

icont = (0) first run, (1) continued run after a  stop of previous

iquies = (0) particle initialization on cells, (1) on orbits

eori = particle species index (1) electron, (2) ion, (3) impurity

npart = particle index in each particle loop.



:file:`elmGeometry.f90`
~~~~~~~~~~~~~~~~~~~~~~~

This module contains data about system geometry.


:file:`elmGrid.f90`
~~~~~~~~~~~~~~~~~~~

Generates grid, volume elements etc. using *grid.inp* input.


:file:`elmGyroFunctions.f90`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This module contains functions related to gyroAverages.


:file:`elmHeating.f90`
~~~~~~~~~~~~~~~~~~~~~~

This includes "background heating" model which has not been in used
recently. It uses following background collision subroutines:

  **subroutine cllstr(xori,r,pitch,v)**

Called by movepr to set collision parametres for iccia

  **subroutine iccll(xr,xv,xori)**

Calculates collisionality and other parameters

  **subroutine iccia(xori,dt,ekin,pit,errflg,enterm,rloc)**

Collisions with fixed boundary.



:file:`elmMatrix.f90`
~~~~~~~~~~~~~~~~~~~~~

This is a fake of the AVL system using the old methods, just to test
if the AVL is breaking the code.



:file:`elmParallel.F90`
~~~~~~~~~~~~~~~~~~~~~~~

  **subroutine elmMatrixSplittingInit(g)**

Fills data for matrix splitting (kind of PESSL's part_block). In case
g=dnxyz is not multiple of number proc, some procs take an extra
row. In the Domain decomposition version, tha group of processors take
care of a domain. :math:`g \propto n` Domain There may be processors
with more lines, but repeated in all domains so that all domains have
same size.  g(in) = matrix dimension.


:file:`elmParticles.f90`
~~~~~~~~~~~~~~~~~~~~~~~~

Module containing information and operations on particles.


:file:`elmRandom.F90`
~~~~~~~~~~~~~~~~~~~~~

Module for speeding up randon number use (and reuse). Used for
encapsulation of the random generator. This modue can use either GSL,
ACML, ESSL and SUN functions.

:file:`elmSolver.F90`
~~~~~~~~~~~~~~~~~~~~~

Module for managing the solver library.


:file:`elmTag.f90`
~~~~~~~~~~~~~~~~~~

Tagging for particle redistribution.


:file:`elmTime.f90`
~~~~~~~~~~~~~~~~~~~

Subroutine for handling several time counters.


:file:`epara.f90`
~~~~~~~~~~~~~~~~~

Implicit calculation of the parallel component of the E field.

Others are calculated in eprep.


:file:`firstt.f90`
~~~~~~~~~~~~~~~~~~

This subroutine initializes particle coordinates uniformly in the
torus and in the velocity space according to a maxwellian. Electrons
are positioned on the ion larmor circles of each ion. The coordinates
are either used as such or as an input for orbit initialization. Some
key variables are

*dr=separation* of equidistant radial init.positions

*dang=separation* of equidistant poloidal init. angles

*prho=ion* larmor radius.


:file:`funcs.f90`
~~~~~~~~~~~~~~~~~

     **subroutine fcn(tim,yy,yprime,errflg)**

This is used in the Runge-Kutta integration this subroutine calculates
the right-hand sides for the guiding-center equations (called by
subroutine runge).


:file:`ibmatxy.f90`
~~~~~~~~~~~~~~~~~~~

  **subroutine ibmatxy(pn,rn,fac,ff,efix,matax,matay,c2p,c2r,kinw,batw)**

This subroutine calculates the coefficients for implicit treatment of
the polarization drift in the potential matrix.  It has to be
consistent with eprep calculation of electric field. Interpolations
using (r,theva) since we need derivatives along them.

:file:`iccom.f90`
~~~~~~~~~~~~~~~~~

Here come global variables that havent been classified to any of the existing modules.


:file:`inipos.f90`
~~~~~~~~~~~~~~~~~~

This subroutine is called for each particle in the beginning of the
step to restore its parameters. at first time step it calls firstt to
make the base restoring. Some key parameters are:

*qi=weight* factor of the particle

*vpar=parallel* velocity of the particle

*vper=perpendicular* velocity

*v=velocity*

*r=radius*

*theta=poloidal (polar) angle*

*theva=poloidal (white-chance) angle*.


:file:`initia.f90`
~~~~~~~~~~~~~~~~~~

This subroutine reads the input data from *icri.inp* and specifies a
number of particle and grid parameters required in computations. It
also allocates memory space for the arrays dependent on grid data and
specifies parameters for the background magentic field. Random
distributions are specified for thermal velocity reinitialization
(vv), for neutral ionization distribution in radius (fneu), and for
ionization energy loss for electrons (enfr). Particle weights are
defined for particle ensembles having both equal (vap=1) and
distributed weights (vap=0). Some key variables are:

*deltax=grid* size in radial direction

*deltay(i)=grid* size in poloidal direction (radially dependent)

*deltaz=grid* size in qb field aligned coordinate

*dny(i)=number* of poloidal cells at the radial index i

*blckSizes=size* of the gk coefficient matrix to be stored in each processor

*qie=electron* weight for vap=1

*l0r(i,k)=number* of particle initialization positions in poloidal direction at the radial grid index i for the particle
k.

*tshft(i)=number* of poloidal cells the field aligned coordinate traverses over one toroidal circulation.



:file:`lhOperator.f90`
~~~~~~~~~~~~~~~~~~~~~~

This module tries to encapsulate the calculations related to LH heating (not in use and probably not up to date)


:file:`moveipr.f90`
~~~~~~~~~~~~~~~~~~~

This subroutine initializes the particles on the uncollisional
neoclassical ion orbits in a torus. The orbits are calculated
numerically from the guiding-center motion of each particle. The orbit
integration is started with the particle positions calculated in the
inipos/firstt codes. The electrons are positioned on the ion larmor
circles (one electron for each ion). If vap=1, only two ion-electron
pairs (with equal species/dependent weights) are stored on the
orbit. If vap=0, nbs pairs (with varying weights) are stored. With
adiabatic electrons (adiab=1) no electron initialization is made.

**subroutine saveipr(doElec)**

Saves particle data for ions and places their electrons.



:file:`movepr.f90`
~~~~~~~~~~~~~~~~~~

This subroutine advances the particles in the guiding-center motion,
and makes actions (reflections or reinitialization) in case of radial
boundary crossing. cx and ionization losses and rf interactions are
also accounted for. Some key varables are:

*epo(i,npart)=perpendicular*
    electric field components of the ion
    after the previous step (epoz for the impurity ion).

*tstep=time step*
    used for each substep of the global step cint

*iscoop=(0)*
    no reinitialization *(1)* reinitialization in scoop needed.



:file:`noislv.f90`
~~~~~~~~~~~~~~~~~~

     **subroutine noislv(ntor)**

This subroutine collects the particle charge density arrays and the
coefficient matrix elements for the gk poisson equation as well as the
boundary data. The coefficient elements are stored in matas array (to
be summed after the particle loops from all processors). The matas
array filling is done at each time step for each particle after the
guiding-center advancing of the particles (done at movepr code) and
with ions takes the major cpu consumption of the code. Ion
contributions come from polarization drift and electron contributions
come from the parallel (along the b field) electron acceleration by
the electric *field.some* key variables are:

kref=
     (0) ion larmor circle inside the calculation region
     (1) outside: thus reflection of circle needed (code repeated for
         ..h. variables)

dnsvct=array for particle charge densities in the grid

increv=weight factor for density filling


:file:`outend.f90`
~~~~~~~~~~~~~~~~~~

This subroutine is for outputting profiles and distributions of
different variables resulting from the code at each time step.


:file:`polari.f90`
~~~~~~~~~~~~~~~~~~

     **subroutine polari**

This subroutine advances ions according to the polarization drift. It
also stores the perpendicular electric field (edrmem etc) to be used
in gk matrix filling in the noislv code. The storing is done into epo
(etc) arrays just after the polari call at the movepr code.



:file:`redistPart.f90`
~~~~~~~~~~~~~~~~~~~~~~

This subroutine moves particles to the domain they belong.



:file:`runge.f90`
~~~~~~~~~~~~~~~~~

This subroutine performs the fourth order runge-kutta integration of
the guiding center equations. Option for predictor-corrector
integrator is also included (this was tested to calculate similarly,
but was not significantly faster), but it is not updated and is not
anymore compatible with the rest of the code.



:file:`scoop.f90`
~~~~~~~~~~~~~~~~~

     **subroutine scoop**

This subroutine ionizes a neutral at a randomly chosen position
(according to a given neutral radial profile), and thus reinitializes
electron/ion pairs outflown (and temporarily reinitialized at the
outer boundary at lossre code) from the outer radial
*boundary.electron* is positioned on the larmor circle of the
ion. *scoop* is called with unequal particle weights (vap=0). Thus the
weights are redefined for the new ion/electron pair with the
*pspre,pspri*, and *psprz* variables. toroidal/poloidal angles and
pitch are chosen randomly. Some key variables are; *nlo(eori)=number*
of outflown eori particle species during the previous step.

*mineori=species* with least outflown particles (either electron or ion)

*maxeori=species* with largest outflown particles

*psco(i,eori)=weight* of i'th outflown eori particle from movepr code.


:file:`scoopv.f90`
~~~~~~~~~~~~~~~~~~

This subroutine is called with vap=1.

     **subroutine scoopv**

this subroutine ionizes a neutral at a randomly chosen position
(according to a given neutral radial profile), and thus reinitializes
electron/ion pairs outflown (and temporarily reinitialized at the
outer boundary at lossre code) from the outer radial
*boundary.electron* is positioned on the larmor circle of the
ion. *scoopv* is called with equal particle weights (vap=1). Thus the
weights are unchanged. Toroidal/poloidal angles and pitch are chosen
randomly. Some key variables are; *nlo(eori)=number* of outflown eori
particle species during the previous step.

:file:`spaslv.f90`
~~~~~~~~~~~~~~~~~~

This subroutine inverts the gyrokinetic equation coefficient matrix
and solves the equation for the potential potmat.
