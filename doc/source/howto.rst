=======
HOWTO's
=======

StarPU notes
============

Building on taito-gpu cluster
-----------------------------


.. code-block:: bash
  
   wget http://starpu.gforge.inria.fr/testing/starpu-1.3/starpu-nightly-latest.tar.gz
   cd starpu-1.3.1.99/
   mkdir build
   cd build
   ../configure --prefix=$HOME/starpu --without-hwloc

Jupyter and Julia
=================


Installation on taito-shell
---------------------------

.. code-block:: bash

   module load python-env/3.5.3
   pip3 install --user jupyter
   export PATH=~/.local/bin:$PATH
   module load julia
   JUPYTER=$(which jupyter) julia
   julia> import Pkg; Pkg.add("IJulia")



GEMPIC
======

Installation
------------

.. code-block:: julia

   julia> using Pkg
   julia> Pkg.clone("https://github.com/JuliaVlasov/VlasovBase.jl.git")
   julia> using VlasovBase
   julia> Pkg.clone("https://github.com/JuliaVlasov/GEMPIC.jl.git")
   julia> using GEMPIC
   julia> import Pkg; Pkg.add("Plots")
   julia> import Pkg; Pkg.add("FFTW")
   julia> import Pkg; Pkg.add("FastGaussQuadrature")
   julia> import Pkg; Pkg.add("Distributions")
   julia> import Pkg; Pkg.add("StatsPlots")
   julia> import Pkg; Pkg.add("Turing")
   julia> import Pkg; Pkg.add("BenchmarkTools")
   using IJulia; notebook()
