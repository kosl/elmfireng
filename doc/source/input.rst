Input file description
----------------------

:file:`fcri.inp`
~~~~~~~~~~~~~~~~

.. highlight:: fortran

Main code parameters are in :file:`icri.inp` as FORTRAN namelists. 

For example:

::

   &title
    caseName='JET-bs'
   /

caseName
   bootstrap current case

elm1
^^^^

::

    &elm1
     r0=2.9056, a=0.9257, aw=0.9257, ltheta=3.141,
     bt=1.7, ct=1.37d6, ea=2.1, vloop=0d0
    /

Section for geometric parameters or engineering parameters. 

r0 
    major radius
a 
    minor radius at the LCFS
aw 
    minir radius of the wall
ltheta 
    angle poloidal position of the limiter
bt 
    toroidal magnetif field
ct 
    toroidal current
ea 
   inert?
vloop 
   loop voltage *inert* mostly infered from input profiles of toroidal 
   current  ?

elm2
^^^^
::

    &elm2
     dnx=184, dnym=200, dnz=8,
     boxnx=14, boxny=14, boxnz=2, boxR=1, boxP=4,
     inx=182
    /

Section for grid.

dnx 
    number of subdivision in minor radius direction (a) toroidal direction
dnym 
    max number of subdivisions in poloidal direction. 
    Outermous number of subdivisions.
dnz 
    Number of subdivisions in toroidal direction. Number of sectors.
boxnx 
    Number of cells we are looking for charge rings in Larmor radius size. 
    Minor radius direction.
boxny 
    poloidal direction
boxnz 
    in toroidal direction
boxR  
    ?
boxP  
    ?
inx 
    Number of radial points in input profile :file:`prof.inp`

elm3
^^^^
::

    &elm3
     tint=3e-8, ttint=4.8006e-4,
     nene=10, nbs=60,
     nchain=21000, queuetime=638000
    /

tint 
    time step 
ttint 
    max time (stop time if reached)
nene 
    number of timestep between each output files (stride)
nbs 
    ?
nchain 
    number of the chaining files from which you can restart simulation
queuetime 
    number of seconds after the simulation is stopped

elm4
^^^^
::

    &elm4
     m=285,285,285, l0=37,37,37, nz=8,8,8, n=13,13,13
     eorim=2, adiab=0, pni=4, vap=1,
     plmass=5.485799e-4,2.d0,12., plznum=-1,1,6
    /

m, l0, nz, n 
    are numbers to initialise particle markers. 
    Subdivisions in phase space. Usually they are not changed.
eorim 
    number of ion species
adiab 
    flags wheter electrons are adiabatic or drift-kinetic
pni 
    is the number of subparticles on the Larmor ring. Usually 4.
vap 
    flag not changed usually (expert only)
plmass 
    mass of species (list electron Deuterium, carbon).
plznum 
    charges -1 for electron, 1 hydrogen and 6 for Carbon 6+
 
elm5
^^^^
::

    &elm5
     tprof=0,0,0, 
     pltemp=120,120,100,
     tdelta=0.02,0.2,0, tteco=1., tempe=0.043, eg=1.3,
     dprof=0,
     pldens=0.5e20,
     ndelta=0.02, dteco=1., dense=0.02, eb=2.5,
     zfr=0.0042,0.0078,0.0146,0.0179,0.0224,0.0246,0.0251,
     dzfr=0.,0.0115,0.02505,0.04567,0.0588,0.06553,0.08
    /


Section for describing analytical profiles. Nowadays inert and
replaced by :file:`prof.inp`

tprof 
    ??
pltemp
    =120,120,100,
tdelta
    =0.02,0.2,0, tteco=1., tempe=0.043, eg=1.3,
dprof
    =0,
pldens
    =0.5e20,
ndelta
    =0.02, dteco=1., dense=0.02, eb=2.5,
zfr
   =0.0042,0.0078,0.0146,0.0179,0.0224,0.0246,0.0251,
dzfr
   =0.,0.0115,0.02505,0.04567,0.0588,0.06553,0.08

elm6
^^^^
::

    &elm6
     nbin=1, binii=1, binei=1, binee=1,
     mcr=185, mct=50, vmax=6.e7,1.e6,1.e6,
     bgheat=0
    /

Section for binary collisions?

nbin 
    flag that enables collissions
binii 
    flag for ion-ion collissions
binei 
    electron-ion collssions
binee 
    electron-electron col.
mcr 
    Different subdivisions for grouping collissions (radial direction)
mct 
    Toroidal (or poloidal) direction
vmax 
    Different cut-off velocities for all species starting with electron
bgheat 
    flag for background heating. If enabled the collisions are 
    calculated with regard to fix background. 

elm7
^^^^
::

    &elm7
     ineut=1, neutralTemper=0.16d0,
     icx=0, dneu=0.50, dnw=0.02,
     radLoss=.FALSE.
    /

Section for neutrals

ineut 
    flag for enabling neutrals recycling
neutralTemper 
    temerature of recycled particles
icx 
    flag for charge-exchange model (not used usually).
dneu 
    ?
dnw 
    ?
radLoss 
    flag (boolean) for radiation losses model 


elm8
^^^^
::

    &elm8
     ncondd=0, dom=5.8e9, ey=50000., wy=40000., sig=1., wz=0.,
     cgg=0.05, rce=0.05, rde=0.03
    /

Section for ?. Used as is.
    
elm9
^^^^
::

    &elm9
     iquies=2, isat=12345, useDnav00=.TRUE., noiter=0, e2=5
    /
   
Section for 

iquies 
    flag for quiscent initialisation to avoid large transients at the start 
    of simulation.
isat 
    inert
useDnav00 
    ?
noiter 
    ?
e2
    ?

elm10
^^^^^
::

    &elm10
     fullDiag=1,
     dnr=5, dnp=6, dnen=5, dnpi=5
    /
    
Diagnosticgsflags

fullDiag all outputs are made. If 0 then only small number of output written.
dnr
dnp
dnen
dnpi



:file:`prof.inp` 
~~~~~~~~~~~~~~~~~

is having the following format:

::

  0.0000000e+00   0.0000000e+00   4.3476372e+19   0.0000000e+00   3.7379750e+03   3.7379750e+03   3.7379750e+03
  1.7700495e-02   1.3465142e+03   4.3468170e+19   0.0000000e+00   3.7381894e+03   3.7381894e+03   3.7381894e+03

Columns are:

1. minor radius
2. toroidal current
3. electron density
4. impurity density
5. electron temperature
6. main ion temperature
7. impurity temperature

File is usualy generated by ASTRA code. From FT-2 tokamak, for example.


:file:`grid.inp`
~~~~~~~~~~~~~~~~
::

  0.00000000e+00    1
  7.16710579e-03    8
  1.37108064e-02   14
  2.02545964e-02   20

1. minor radius of the grid points
2. number of subdivisions at the corresponding minor radius. Should
   not be greater than ``dnym``
