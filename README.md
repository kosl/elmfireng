**ELMFIRE in a nutshell**

Part of *HPCE3 Refactoring Elmfire gyrokinetic code for multiscale
processing (2001475)* project that is related to PRACE and EuroHPC
long term goal in addressing code scalability in various scientific
domains. Through HPCEuropa3 funding we would like to provide new
insights in tackling heterogenous systems and load imbalances that has
been observed in the gyrokinetic codes first focusing to the ELMFIRE
code developed in Aalto University.

---

## Clone a repository

Before cloning the repository register your SSH key in your bitbucket profile.
To clone use the following commands:

    git clone git@bitbucket.org:<yourusername>/elmfireng.git
    cd elmfireng
    git checkout feature/doc

Use these steps to clone from SourceTree, our client for using the
repository command-line free. Cloning allows you to work on your files
locally. If you don't yet have SourceTree, [download and install
first](https://www.sourcetreeapp.com/). If you prefer to clone from
the command line, see [Clone a
repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead
and add a new file locally. You can [push your change back to
Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg),
or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ)
and [push from the command
line](https://confluence.atlassian.com/x/NQ0zDQ).

## Documentation

To compile documentation on CSC computers use Python based Sphinx generator.


To load and prepare environment:

    module load python-env/3.5.3
    pip3 install --user sphinx_rtd_theme
    export PATH=${HOME}/.local/bin:${PATH}

On Debian Linux 10:

   apt-get install python3-sphinx-rtd-theme
   apt-get install texlive latexmk texlive-latex-extra				

To recompile and view the documentation:

    make -C doc html latex # or just latexpdf
    xdg-open doc/build/html/index.html
    make -C doc/build/latex all-pdf
    xdg-open doc/build/latex/ELMFIREng.pdf

