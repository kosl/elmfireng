SIMPIC - Simple 1D PIC prototype
================================

SIMPIC is extracted from [OOPD1]_ source code to demonstrate [StarPU]_
possibilities.

The code is a simple one-dimensional electrostatic code (meaning
:math:`\nabla\times\mathbf{B}=\partial\mathbf{B}/\partial t \approx 0`
so that :math:`\mathbf{E}=-\nabla\phi`), with self and applied
electric fields *E* directed along coordinate *x*. There are no
variations in *y* or *z* directions. The plane parallel problem
cosnsists of single (electron) species.

Building
~~~~~~~~

MPI C++ compiler is required to build and run SIMPIC.

.. code-block:: bash

  git clone git@bitbucket.org:kosl/elmfireng.git
  cd elmfireng
  git fetch && git checkout feature/simpic
  cd simpic
  make
  make html
  xdg-open build/html/index.html

Running with MPI
~~~~~~~~~~~~~~~~

.. code-block:: bash

  ./runsimpic.sh 2> run.log
  mpirun -np 4 a.out -ppc 100 -ncpp 64 -nt 200 -dtfactor 0.001 -lhsv 25000
  rank 0 of 4 processors
  MPI version: 3.1, MPI_Wtime precision: 1e-09
  t=1.12e-09, dt=5.6e-12, ntimesteps=200, density=1e+13, wp=1.79e+08,
  np2c=3.91+08 6400 particles, ng=65, nc=64, L=1, Ll=0.25, dx=0.00390625,
  area=1
  
  Timings:
  Particles time: 0.111255 [5.0%]
  Fields time: 0.000620347 [0.0%]
  MPI time: 0.154564 [6.9%]
  Diagnostics time: 1.93947 [86.9%]
  Initialization time: 0.00460355 [0.2%]
  
  Total time: 2.23218 (2.21051 [99.0%])
  569640 (1.14291e+07) particles/second
  		


Diagnostics
~~~~~~~~~~~

Change to `diagnosticsflag = true` in :file:`simpic.cpp` to get
diagnostic :file:`.dat` files of:

  - density
  - E electric field
  - phi potential
  - vxx (phase space)
  - nt is time evolution of number of particles per processor

File format for each processor is::

  time relative_position value

Defining `DEBUG` symbol in :file:`global.h` produces additional
prints to `stderr` stream.
  
.. note::

   Timings are not representative when defining `DEBUG` and
   `diagnosticsflag = true`.


Variables
~~~~~~~~~

.. code-block:: bash
		
 NT=200  # of time steps
 PPC=100 # of particles per cell
 CPP=64  # of cell per process
 DTFACTOR=0.001 #defines a fraction of dt vs. time steps from plasma frequency;
		#must be positive
 NPROC=4 # of processors
 LHSV=25000 #applied voltage on left-hand side; RHS is grounded;

`density = 1.E13`
  density of electrons (:math:`n`)
`epsilon = 8.85E-12`
  permititvity of free space (:math:`\epsilon_0`)
`nc`
  number of cells per processor (CPP=64)
`ng`
  number of gridpoints is always one more than number of cells. ng=nc+1
`npart`
  number of (super)particles per processor
  = number of cells * number of particles per cell
`L`
  total length of the system
`q`, `m`
  elementary charge and electron mass
`xl, xr`
  *x* position of left and right bounaries for each processor
`Ll`
  length of the system for each processor
`dx`
  length of each cell :math:`dx=Ll/nc`,
  where `nc` is number of cells per processor
`nl`
  rank of the processor on the left.
  Process 0 has last process on the left in a cyclic manner.
`nr`
  rank of the processor on the right.
  Last process has process 0 on the right in cyclic manner.
`np2c`
  number of (electron) particles per cell
  = density * volume of each processor covers/number of particles per processor
`wp`
  electron plasma frequency :math:`\omega_p^2 = \frac{n q^2}{\epsilon_0 m}`
`dtfactor=0.001`
  fraction of plasma frequency time :math:`\tau = 1/\omega_p` to be used as 
`dt`
  time step :math:`\delta t=dtfactor\cdot\tau`
`pdata[2*nproc*npart]`
  phase-space data array of particle *x* positions and *v* velocities
  alternated. Index `j` is *x* position and `j+1` is velocity.
`lhsbuf[2*nproc*npart], rhsbuf[2*nproc*npart]`
  buffers of particle positions and velocities on the left and right.
`narray[ng]`
  density array
`phiarray[ng]`
  potential array
`Earray[ng]`
  Electric field array
`nback[ng]`
  not used. intert. backup.
`scale`
  generic scaling of differences equation that usually multiplies
  with :math:`1/\Delta x` or similar weigting.
  
Main loop
~~~~~~~~~

`moveparticles(dt)`
     Computes from electric field *E* new velocity
     for each particle and moves it to new position. If the particles
     moves out of processor bounds [xl, xr] then it is moved to left
     or right hand side buffer for communication to neigboring
     processors.
`communicateparticles()`
     Each processor sends particles to the left and right. Before
     communition of arrays starts, sizes are exchanged.  
`advancefields(dt)`
     Tridiagonal matrix of Poisson equation
     :math:`\phi_{j+1}-2\phi_j+\phi_{j-1}=p_j`
     is solved with Gaussian elimination  [BiLa85]_ by each processor
     and then fields are communicated to all processors. Then Laplace
     equation is summed to
     get the potential :math:`phi`-array. From :math:`\phi` negative gradient
     gives E-array.

StarPU notes
============

Building on taito-gpu cluster
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


.. code-block:: bash
  
   wget http://starpu.gforge.inria.fr/testing/starpu-1.3/starpu-nightly-latest.tar.gz
   cd starpu-1.3.1.99/
   mkdir build
   cd build
   ../configure --prefix=$HOME/starpu --without-hwloc

     

.. only:: html	   

   .. rubric:: Rerefences

.. [BiLa85] C.K. Birdsall, A.B Langdon,
            *Plasma Physics via Computer Simulation*,Adam Hilger press,
	    1991, p.446
.. [OOPD1]  https://ptsg.egr.msu.edu/#Software
.. [StarPU] http://starpu.gforge.inria.fr/
	    

